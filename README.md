# Godot Module Music Plugin

Allows the use of module music formats in Godot, such as .mod or .xm files.